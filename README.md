# Wireworld

[Wireworld][wireworld-wikipedia] is a cellular automata, like the [Conway's Game of Life][gol-wikipedia],
but instead of having only two states (_alive_ and _dead_), it has four:
- ground
- wire
- electron head
- electron tail

This project is _heavily_ inspired by a [nim port][wireworld-nim], and is in the same time an occasion to
do some Rust, learn a bit of SDL (or maybe another rendering library), and a gentle introduction to cellular
automatas.

# Build

This project uses `rust`, so you'll need it. Check your package manager for installing it, or look
at [`rustup`][rustup].
You'll also need `sdl2` installed, again check your package manager.
Then just clone this repo and build it:
```bash
git clone https://gitlab.com/justinrlle/wireworld-rust
cd wireworld-rust
cargo run --release
```

# Todo

Doc:
- All of `ParseError`
- All of `Serializer`
- All of `SerializerOpts`
- Some examples of `World`

`World`:
- implement `Index`
- fix the `iter` method


[wireworld-wikipedia]: https://en.wikipedia.org/wiki/Wireworld
[gol-wikipedia]: https://en.wikipedia.org/wiki/Conway's_Game_of_Life
[wireworld-nim]: https://github.com/JHonaker/wireworld-nim
[rustup]: https://rustup.rs/
