use super::{ParseError, World};

static WORLD_STR: &str = r"           
    ####   
   #    0  
   #    #  
  ###   #  
  # #    # 
   #    # #
   #    ###
   #     # 
    #####  ";

#[test]
fn parse_world() {
    let world = should_parse(WORLD_STR);
    assert_eq!(world.width, 11);
    assert_eq!(world.height, 10);
}

#[test]
fn inconsistant_widths() {
    let world_str = "###\n####";
    let err = should_not_parse(world_str);
    if let ParseError::InconsistantWidth {
        got,
        expected,
        line,
    } = err
    {
        assert_eq!(got, 4);
        assert_eq!(expected, 3);
        assert_eq!(line, 1);
    } else {
        panic!("Not the right error returned");
    }
}

#[test]
fn wrong_char() {
    let world_str = "## \n-# ";
    let err = should_not_parse(world_str);
    if let ParseError::UnknownChar { got, .. } = err {
        assert_eq!(got, '-');
    }
}

#[test]
fn iterator_delivers() {
    let world = should_parse(WORLD_STR);
    for (state, x, y) in world.iter() {
        assert_eq!(state, world[(x, y)]);
    }
}

fn should_parse(s: &str) -> World {
    World::parse_str(s).expect("Could not parse a valid world")
}

fn should_not_parse(s: &str) -> ParseError {
    World::parse_str(s).expect_err("Parsing should have failed")
}
