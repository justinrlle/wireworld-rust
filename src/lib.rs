#![cfg_attr(feature = "doc", feature(external_doc))]
#![cfg_attr(feature = "doc", doc(include = "README.md"))]

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(test)]
mod test;

use std::{
    fmt,
    ops::{Index, IndexMut},
    str::FromStr,
};

static DEFAULT_OPTS: SerializerOpts = SerializerOpts {
    ground: ' ',
    wire: '#',
    head: '0',
    tail: 'o',
};

/// The state of a cell.
///
/// In the cellular automata of wireworld, there is 4 states. The presence of the [`Ground`] one is
/// mainly useless, because the [`World`] struct doesn't use it.  But it still as its uses, and
/// allow one to have a complete code in all cases, even if a path is never reached.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum State {
    /// The ground state. In the rules of wireworld, never changes, but if you want some
    /// interactivity to your program (a click on it produces a [`Wire`]), usefull for matching
    /// against.
    Ground,
    /// The wire state, usually presented yellow. The wire cell is the support for the [`Head`] and
    /// the [`Tail`] states, they can exist only when a cell is a wire.
    Wire,
    /// Electron head, ususally colored blue. An head becomes automatically a [`Tail`], but not all
    /// [`Wire`] becomes a head when next to one.
    Head,
    /// Electron tail, next state of a [`Head`]. This state exists so that an electron does not
    /// go backward.
    Tail,
}

use State::{Ground, Head, Tail, Wire};

/// How to read/print a world from/to a str.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct SerializerOpts {
    ground: char,
    wire: char,
    head: char,
    tail: char,
}

/// Wrapper structs which handles the printing of a world with a [`SerializerOpts`](SerializerOpts)
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Serializer<'a> {
    opts: &'a SerializerOpts,
    world: &'a World,
}

/// An instance of a wireworld world. Holds all the states.
///
/// You can currently create one either from given dimensions, and add states one by one after, or
/// you can parse one from an [`str`] (anything implementing `AsRef<str>` in fact). Then you can
/// either call [`process()`] on it, to make it evolve to the next global state,
/// or you can set manually a state at arbitrary coordinates. Finally, to print it to a string,
/// you'll use the [`as_serializer()`]/[`as_serializer_with()`] methods, which will return a struct
/// implementing [`std::fmt::Display`].
///
/// # Examples
///
/// Let's parse a file, process it, and print it.
/// ```
///# use std::fs;
///# use wireworld::*;
///# fn main() -> Result<(), Box<dyn std::error::Error>> {
/// let world_str = fs::read_to_string("worlds/basic.txt")?;
/// let mut world = World::parse_str(&world_str)?;
/// world.process();
/// println!("{}", world.as_serializer());
///# Ok(())
///# }
/// ```
///
/// [`process()`]: World::process
/// [`as_serializer()`]: World::as_serializer
/// [`as_serializer_with()`]: World::as_serializer_with
#[derive(Debug, Clone, Default, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct World {
    width: usize,
    height: usize,
    states: Vec<State>,
}

/// The only error possible in this library, a parsing error.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum ParseError {
    InconsistantWidth {
        expected: usize,
        got: usize,
        line: usize,
    },
    UnknownChar {
        got: char,
        chars: (char, char, char, char),
    },
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ::std::fmt::Write;
        f.write_char(self.as_char())
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseError::InconsistantWidth {
                expected,
                got,
                line,
            } => write!(
                f,
                "On line {}, expected a width of {}, got {} from previous lines",
                line, expected, got
            ),
            ParseError::UnknownChar { got, chars } => write!(
                f,
                "Got char `{}`, expected one of `{}, {}, {}, {}`",
                got, chars.0, chars.1, chars.2, chars.3
            ),
        }
    }
}

impl std::error::Error for ParseError {}

impl State {
    /// Return the right state for the given char, using the default charset.
    ///
    /// # Errors
    ///
    /// Will return a [`ParseError::UnknownChar`] error if the char is not known in the default
    /// charset.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let state = State::from_char(' ')?; // the default char for a ground.
    /// assert_eq!(state, State::Ground);
    /// let state = State::from_char('a'); // 'a' is not in the default charset.
    /// assert!(state.is_err());
    ///# Ok(())
    ///# }
    /// ```
    pub fn from_char(c: char) -> Result<State, ParseError> {
        match c {
            c if c == SerializerOpts::default_ground() => Ok(Ground),
            c if c == SerializerOpts::default_wire() => Ok(Wire),
            c if c == SerializerOpts::default_head() => Ok(Head),
            c if c == SerializerOpts::default_tail() => Ok(Tail),
            _ => Err(ParseError::UnknownChar {
                got: c,
                chars: (
                    SerializerOpts::default_ground(),
                    SerializerOpts::default_wire(),
                    SerializerOpts::default_head(),
                    SerializerOpts::default_tail(),
                ),
            }),
        }
    }

    /// Return the right state for the given char, using the specified charset.
    ///
    /// # Errors
    ///
    /// Will return a [`ParseError::UnknownChar`] error if the char is not known in the given
    /// charset.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let opts = SerializerOpts::new(None, ':', '#', None);
    /// let state = State::from_char_with(':', &opts)?; // the char for a wire
    /// assert_eq!(state, State::Wire);
    /// let state = State::from_char('a'); // 'a' is not in the charset.
    /// assert!(state.is_err());
    ///# Ok(())
    ///# }
    /// ```
    pub fn from_char_with(c: char, opts: &SerializerOpts) -> Result<State, ParseError> {
        match c {
            c if c == opts.ground => Ok(Ground),
            c if c == opts.wire => Ok(Wire),
            c if c == opts.head => Ok(Head),
            c if c == opts.tail => Ok(Tail),
            _ => Err(ParseError::UnknownChar {
                got: c,
                chars: (opts.ground, opts.wire, opts.head, opts.tail),
            }),
        }
    }

    /// Return the char representing the state according to the default charset.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let state = State::Tail;
    /// assert_eq!(state.as_char(), 'o'); // the default char for a tail
    ///# Ok(())
    ///# }
    /// ```
    pub fn as_char(self) -> char {
        match self {
            Ground => SerializerOpts::default_ground(),
            Wire => SerializerOpts::default_wire(),
            Head => SerializerOpts::default_head(),
            Tail => SerializerOpts::default_tail(),
        }
    }

    /// Return the char representing the state according to the given charset.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let opts = SerializerOpts::new(None, ':', '#', None);
    /// let state = State::Head;
    /// assert_eq!(state.as_char_with(&opts), '#'); // the given char for a head
    ///# Ok(())
    ///# }
    /// ```
    pub fn as_char_with(self, opts: &SerializerOpts) -> char {
        match self {
            Ground => opts.ground,
            Wire => opts.wire,
            Head => opts.head,
            Tail => opts.tail,
        }
    }
}

impl SerializerOpts {
    pub fn new<T1, T2, T3, T4>(ground: T1, wire: T2, head: T3, tail: T4) -> SerializerOpts
    where
        T1: Into<Option<char>>,
        T2: Into<Option<char>>,
        T3: Into<Option<char>>,
        T4: Into<Option<char>>,
    {
        SerializerOpts {
            ground: ground.into().unwrap_or(DEFAULT_OPTS.ground),
            wire: wire.into().unwrap_or(DEFAULT_OPTS.wire),
            head: head.into().unwrap_or(DEFAULT_OPTS.head),
            tail: tail.into().unwrap_or(DEFAULT_OPTS.tail),
        }
    }

    pub fn default_ground() -> char {
        DEFAULT_OPTS.ground
    }
    pub fn default_wire() -> char {
        DEFAULT_OPTS.wire
    }
    pub fn default_head() -> char {
        DEFAULT_OPTS.head
    }
    pub fn default_tail() -> char {
        DEFAULT_OPTS.tail
    }
}

impl Default for SerializerOpts {
    fn default() -> SerializerOpts {
        DEFAULT_OPTS.clone()
    }
}

impl<'a> Serializer<'a> {
    pub fn of(world: &World) -> Serializer {
        Serializer::new(world, &DEFAULT_OPTS)
    }

    pub fn new(world: &'a World, opts: &'a SerializerOpts) -> Serializer<'a> {
        Serializer { world, opts }
    }
}

impl World {
    /// Construct a world for given dimensions.
    ///
    /// Will also reserve the size for the underlying vector.
    ///
    /// # Examples
    /// ```
    ///# use wireworld::*;
    /// let world = World::new(10, 5);
    /// assert!(world.iter().all(|(s, _, _)| s == State::Ground));
    /// assert_eq!(world.width(), 10);
    /// assert_eq!(world.height(), 5);
    /// ```
    pub fn new(width: usize, height: usize) -> World {
        let states = vec![Ground; width * height];
        World {
            width,
            height,
            states,
        }
    }

    /// Accessor method to get the width of a world.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Accessor method to get the height of a world.
    pub fn height(&self) -> usize {
        self.height
    }

    fn coord(&self, x: usize, y: usize) -> usize {
        x + self.width * y
    }

    fn get_next_state(&self, x: usize, y: usize) -> State {
        match self.states[self.coord(x, y)] {
            Ground => Ground,
            Head => Tail,
            Tail => Wire,
            Wire => {
                let count = self.head_neighboors(x, y);
                if count == 1 || count == 2 {
                    Head
                } else {
                    Wire
                }
            }
        }
    }

    /// Run one iteration of the world, and update the world based on it.
    ///
    /// # Examples
    /// ```
    ///# use wireworld::*;
    /// let mut world = World::new(10, 10);
    /// world[(2, 2)] = State::Wire;
    /// world[(2, 3)] = State::Head;
    /// world.process();
    /// assert_eq!(world[(2, 2)], State::Head);
    /// assert_eq!(world[(2, 3)], State::Tail);
    /// ```
    pub fn process(&mut self) {
        let mut states = vec![Ground; self.width * self.height];
        for i in 0..self.width {
            for j in 0..self.height {
                states[self.coord(i, j)] = self.get_next_state(i, j);
            }
        }
        self.states = states;
    }

    /// Count the number of electron heads around a point.
    ///
    /// If the state at the coordinates is not a wire, will be a no-op.
    /// **TODO**: must be part of public API? What does it brings?
    /// # Examples
    /// ```
    /// // TODO
    /// ```
    pub fn head_neighboors(&self, x: usize, y: usize) -> u32 {
        match self.states[self.coord(x, y)] {
            Wire => {}
            _ => {
                return 0;
            }
        };
        let mut res = 0;
        let x1 = if x == 0 { 0 } else { x - 1 };
        let x2 = x + 2;
        let y1 = if y == 0 { 0 } else { y - 1 };
        let y2 = y + 2;
        for i in x1..x2 {
            for j in y1..y2 {
                if i < self.height && j < self.width {
                    res += match self.states[self.coord(i, j)] {
                        Head => 1,
                        _ => 0,
                    };
                }
            }
        }
        res
    }

    /// Returns an iterator over the states and theirs coordinates.
    ///
    /// The type of the item is a ([`State`], [`usize`], [`usize`]), as `(state, width, height)`
    ///
    /// # Examples
    /// ```
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let world_str = include_str!("../worlds/basic.txt");
    /// let world = World::parse_str(world_str)?;
    /// let mut iter = world.iter();
    ///
    /// assert_eq!(iter.next(), Some((State::Ground, 0, 0)));
    /// let mut iter = iter.skip(12);
    /// assert_eq!(iter.next(), Some((State::Wire, 3, 1)));
    ///# Ok(())
    ///# }
    /// ```
    pub fn iter(&self) -> Iter<'_> {
        Iter::new(self.states.iter().enumerate(), self.width)
    }

    /// Parse a world from a string, using the default charset.
    ///
    /// # Errors
    ///
    /// Will return a [`ParseError::InconsistantWidth`] error if two lines are not of the same
    /// length, or an [`ParseError::UnknownChar`] if there is an unknown char. Refer to
    /// [`State::from_char`] for more info on how it could fail.
    ///
    /// # Examples
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let world_str = include_str!("../worlds/basic.txt");
    /// let world = World::parse_str(world_str)?;
    /// assert_eq!(world.width(), 10);
    /// assert_eq!(world.height(), 10);
    /// assert_eq!(world[(3, 1)], State::Wire, "no wire at (3, 1)");
    /// assert_eq!(world[(7, 2)], State::Head, "no head at (7, 2)");
    ///# Ok(())
    ///# }
    /// ```
    pub fn parse_str<T: AsRef<str>>(s: T) -> Result<World, ParseError> {
        World::parse_str_with(s, &SerializerOpts::default())
    }

    /// Parse a world from a string, using the given charset.
    ///
    /// # Errors
    ///
    /// Will return a [`ParseError::InconsistantWidth`] error if two lines are not of the same
    /// length, or an [`ParseError::UnknownChar`] if there is an unknown char. Refer to
    /// [`State::from_char`] for more info on how it could fail.
    ///
    /// # Examples
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// // change the wire char
    /// let world_str = include_str!("../worlds/basic.txt").replace('#', ":");
    /// let opts = SerializerOpts::new(None, ':', None, None);
    /// let world = World::parse_str_with(&world_str, &opts)?;
    /// assert_eq!(world.width(), 10);
    /// assert_eq!(world.height(), 10);
    /// assert_eq!(world[(3, 1)], State::Wire, "no wire at (3, 1)");
    /// assert_eq!(world[(7, 2)], State::Head, "no head at (7, 2)");
    ///# Ok(())
    ///# }
    /// ```
    pub fn parse_str_with<T: AsRef<str>>(s: T, opts: &SerializerOpts) -> Result<World, ParseError> {
        let size = s.as_ref().matches(|c: char| !c.is_whitespace()).count();
        let world = World {
            width: 0,
            height: 0,
            states: Vec::with_capacity(size),
        };
        let mut world =
            s.as_ref()
                .lines()
                .enumerate()
                .fold(Ok(world), |try_world, (i, line)| {
                    if let Ok(mut world) = try_world {
                        if world.width == 0 {
                            world.width = line.len();
                        } else if world.width != line.len() {
                            return Err(ParseError::InconsistantWidth {
                                got: line.len(),
                                expected: world.width,
                                line: i,
                            });
                        }
                        let states = line
                            .chars()
                            .map(|c| State::from_char_with(c, opts))
                            .collect::<Result<Vec<_>, _>>()?;
                        world.states.extend(states);
                        Ok(world)
                    } else {
                        try_world
                    }
                })?;
        let height = world.states.len() / world.width;
        world.height = height;
        Ok(world)
    }

    /// Returns a [`Serializer`] with the default charset, allowing to print this world.
    ///
    /// The [`Serializer`] is a struct implementing [`std::fmt::Display`], so you can use it in any
    /// `format_args!()` invocation.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// let world_str = include_str!("../worlds/basic.txt");
    /// let world = World::parse_str(world_str)?;
    /// assert_eq!(world_str.trim(), format!("{}", world.as_serializer()).trim());
    ///# Ok(())
    ///# }
    /// ```
    pub fn as_serializer(&self) -> Serializer {
        Serializer::of(self)
    }

    /// Returns a [`Serializer`] with a given charset, allowing to print this world using specified
    /// chars.
    ///
    /// The [`Serializer`] is a struct implementing [`std::fmt::Display`], so you can use it in any
    /// `format_args!()` invocation.
    ///
    /// # Examples
    ///
    /// ```
    ///# use std::fs;
    ///# use wireworld::*;
    ///# fn main() -> Result<(), ParseError> {
    /// // replace the wire char
    /// let world_str = include_str!("../worlds/basic.txt").replace('#', ":");
    /// let opts = SerializerOpts::new(None, ':', None, None);
    /// let world = World::parse_str_with(&world_str, &opts)?;
    /// assert_eq!(world_str.trim(), format!("{}", world.as_serializer_with(&opts)).trim());
    ///# Ok(())
    ///# }
    /// ```
    pub fn as_serializer_with<'a>(&'a self, opts: &'a SerializerOpts) -> Serializer<'a> {
        Serializer::new(self, &opts)
    }
}

impl Index<(usize, usize)> for World {
    type Output = State;

    fn index(&self, (x, y): (usize, usize)) -> &State {
        self.states.index(self.coord(x, y))
    }
}

impl IndexMut<(usize, usize)> for World {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut State {
        let coord = self.coord(x, y);
        self.states.index_mut(coord)
    }
}

pub use iter::Iter;
mod iter {
    use super::State;
    use std::iter::{DoubleEndedIterator, Enumerate, ExactSizeIterator, FusedIterator};
    use std::slice::Iter as SliceIter;

    /// Iterator struct used for iterating on a world.
    ///
    /// It yeilds a [`State`](super::State) with its associated coordinates, in the following form:
    /// `(State, usize, usize)`, which can also be understood as `(state, x, y)`.
    pub struct Iter<'a> {
        inner: Enumerate<SliceIter<'a, State>>,
        width: usize,
    }

    impl<'a> Iter<'a> {
        pub(crate) fn new(iter: Enumerate<SliceIter<'a, State>>, width: usize) -> Iter<'a> {
            Iter { inner: iter, width }
        }

        fn map_value(&self, (i, state): (usize, &State)) -> (State, usize, usize) {
            (*state, i % self.width, i / self.width)
        }
    }

    impl<'a> Iterator for Iter<'a> {
        type Item = (State, usize, usize);
        fn next(&mut self) -> Option<Self::Item> {
            self.inner.next().map(|v| self.map_value(v))
        }

        fn size_hint(&self) -> (usize, Option<usize>) {
            self.inner.size_hint()
        }
    }

    impl<'a> DoubleEndedIterator for Iter<'a> {
        fn next_back(&mut self) -> Option<Self::Item> {
            self.inner.next_back().map(|v| self.map_value(v))
        }
    }

    impl<'a> ExactSizeIterator for Iter<'a> {}

    impl<'a> FusedIterator for Iter<'a> {}
}

impl<'a> fmt::Display for Serializer<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ::std::fmt::Write;

        for j in 0..self.world.height {
            for i in 0..self.world.width {
                f.write_char(self.world[(i, j)].as_char_with(&self.opts))?;
            }
            f.write_char('\n')?;
        }
        Ok(())
    }
}

/// Just a proxy to the [`World::parse_str()`] static method.
impl FromStr for World {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<World, ParseError> {
        World::parse_str(s)
    }
}
