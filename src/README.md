This library provides types and functions to work with a specific [cellular automaton], called
[wireworld]. If you want more info on what both are, please click on the wikipedia links.

The principal struct is the [`World`] struct. It contains the dimension of the world, and the list
of states.

# Getting started

Add the crate to your dependencies in your `Cargo.toml`:
```toml
[dependencies]
wireworld = { git = "https://gitlab.com/justinrlle/wireworld-rust.git" }
```

And to your `external crate`s in your `lib.rs`/`main.rs`:
```rust
extern crate wireworld;
```

Then, create a world from a string, and run it:
```rust
# extern crate wireworld;
# extern crate failure;
#
use std::fs;
use wireworld::World;
#
# fn main() -> Result<(), failure::Error> {

let mut world = fs::read_to_string("worlds/hello.txt")?.parse::<World>()?;
world.process();
println!("{}", world.as_serializer());
#
# Ok(())
# }

```

# The differents types

## `World`

The [`World`] is the main type, holding the state of the cellular automaton. It goes to the next
step with the method [`World::process()`]. You can also use the [`World::set_state()`] method, for
exemple when responding to user actions, allowing them to add more wire, or to generate an
electron.

You can deserialize a [`World`] from anything implementing `AsRef<str>`, using either the default or
a custom [`SerializerOpts`], and by wraping it in a [`Serializer`], you can print it to a screen.

### Possible improvements

- Rework storage of states:
  Currently, just a [`Vec`] of [`State`]s, but might be simpler to use a [`HashMap`]. Also,
  a [`Vec`] is just a trick to not use a imitate a 2-dimensions array.
- [x] Need an API to mutate the world based on user input, and not only based on processing turns.
- [x] Use a real [`Iterator`] for the [`World::iter()`] method, with the `impl Trait` syntax we loose
  all the trait implementented by [`std::slice::Iter`], the underlying iterator.

## `SerializerOpts`

The [`SerializerOpts`] struct is responsible for descripting how a [`World`] should be read
from/written to a string, with the following set as the default:
```text
ground = ' '
wire = '#'
head = '0'
tail = 'o'
```
> It might change in the future, this is one of the most visible set I found, but it's not perfect.

The simplest way to parse a [`str`] into a [`World`] is to use the [`World::parse_str()`]
method, it will use the default set of [`SerializerOpts`] to parse the string:
```rust
# extern crate wireworld;
# extern crate failure;
#
use std::fs;
use wireworld::World;
#
# fn main() -> Result<(), failure::Error> {

let world_file = fs::read_to_string("worlds/hello.txt")?;
let world = World::parse_str(world_file)?;
println!("{}", world.as_serializer());
#
# Ok(())
# }
```
But if you need any other kind of chars, just create the [`SerializerOpts`] struct, and pass it to
the method [`World::parse_str_with()`]:
```rust
# extern crate wireworld;
# extern crate failure;
#
use std::fs;
use wireworld::{World, SerializerOpts};
#
# fn main() -> Result<(), failure::Error> {

let opts = SerializerOpts::new(None, ':', '#', None);
let world_file = fs::read_to_string("worlds/hello.txt")?;
let world = World::parse_str_with(world_file, &opts)?;
println!("{}", world.as_serializer());
#
# Ok(())
# }
```

The [`SerializerOpts`] is also usefull for the [`Serializer`](#serializer) type.

### Possible improvements

- [x] Allow something like `SerializerOpts::new(None, ':', '#', None)`, where `None` will be replaced
  with the default char.

# `Serializer`

The [`Serializer`] type is a nice way to keep the serializing part of a world independant of the
[`SerializerOpts`] struct. It is okay to need it for parsing, because it's a static method, but for
printing, it won't be logical to need it in the properties of a [`World`]. So the [`Serializer`] is
a wrapper for a [`World`] and a [`SerializerOpts`]. And because of `rust` powerful owernership and
lifetimes, going from a [`World`] to a [`Serializer`] is nearly a no-cost. It is done through the
[`World::as_serializer()`] method, which will use the default [`SerializerOpts`], as always, and you
can use a specific [`SerializerOpts`] with the [`World::as_serializer_with()`] variant of the
method.

### Possible improvements

- [x] Use a ref to a [`SerializerOpts`], and have a static [`SerializerOpts`] to be used in the default
  case. Might make the operation even more no-cost.
- [x] Make something of the [`Cow<World>`](std::borrow::Cow): either use it to mutate the world through
  a [`Serializer`] with a [`Deref`](std::ops::Deref) implementation (looks dirty to me), or remove
  it, if the above statements turns the operation in really something no-cost. (second option choosed)


# Possible improvements

- [x] Need a [`serde`] feature

[cellular automaton]: https://en.wikipedia.org/wiki/Cellular_automaton
[wireworld]: https://en.wikipedia.org/wiki/Wireworld
[`HashMap`]: std::collections::HashMap
[`serde`]: https://docs.rs/serde
