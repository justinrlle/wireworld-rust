use crate::controller::Controller;
use graphics::{types::Color, Context, Graphics};
use wireworld::State;

const GROUND: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
const WIRE: [f32; 4] = [1.0, 0.843, 0.0, 1.0];
const HEAD: [f32; 4] = [0.0, 0.502, 1.0, 1.0];
const TAIL: [f32; 4] = [1.0, 0.251, 0.0, 1.0];
const SPACING: [f32; 4] = [0.251, 0.251, 0.251, 1.0];

pub const TILE_SIZE: f64 = 12.0;

pub struct WorldSettings {
    /// Usually brown, or deep black
    pub ground_color: Color,
    /// Usually yellow
    pub wire_color: Color,
    /// Usually blue
    pub head_color: Color,
    /// Usually red
    pub tail_color: Color,
    /// Usually gray
    pub spacing_color: Color,
    /// Size of a tile in pixels
    pub tile_size: f64,
    /// Number of pixels between two tiles
    pub tile_spacing: f64,
}

impl WorldSettings {
    fn color_of(&self, state: State) -> Color {
        match state {
            State::Ground => self.ground_color,
            State::Wire => self.wire_color,
            State::Head => self.head_color,
            State::Tail => self.tail_color,
        }
    }

    fn dimensions_for(&self, x: usize, y: usize) -> (f64, f64) {
        (
            (x as f64) * (self.tile_size + self.tile_spacing) + self.tile_spacing,
            (y as f64) * (self.tile_size + self.tile_spacing) + self.tile_spacing,
        )
    }
}

impl Default for WorldSettings {
    fn default() -> WorldSettings {
        WorldSettings {
            ground_color: GROUND,
            wire_color: WIRE,
            head_color: HEAD,
            tail_color: TAIL,
            spacing_color: SPACING,
            tile_size: TILE_SIZE,
            tile_spacing: 1.0,
        }
    }
}

pub struct View {
    settings: WorldSettings,
}

impl View {
    pub fn new(settings: WorldSettings) -> View {
        View { settings }
    }

    pub fn draw(&self, controller: &Controller, c: &Context, g: &mut impl Graphics) {
        use graphics::{clear, rectangle::square, Rectangle};
        clear(SPACING, g);
        for (state, x, y) in controller.world.iter() {
            let (x, y) = self.settings.dimensions_for(x, y);
            Rectangle::new(self.settings.color_of(state)).draw(
                square(x, y, self.settings.tile_size),
                &c.draw_state,
                c.transform,
                g,
            );
        }
    }
}

pub fn size_for(width: usize, height: usize) -> (u32, u32) {
    let tile_size = (TILE_SIZE as u32) + 1;
    (
        (width as u32) * tile_size + 1,
        (height as u32) * tile_size + 1,
    )
}
