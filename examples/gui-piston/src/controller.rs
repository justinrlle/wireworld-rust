use piston::input::GenericEvent;
use std::time::{Duration, Instant};
use wireworld::World;

pub struct Controller {
    pub world: World,
    timer: Instant,
    delay: Duration,
}

impl Controller {
    pub fn new(world: World, delay: Duration) -> Controller {
        Controller {
            world,
            timer: Instant::now(),
            delay,
        }
    }

    pub fn event(&mut self, _event: &impl GenericEvent) {
        let now = Instant::now();
        if now.duration_since(self.timer) > self.delay {
            self.world.process();
            self.timer = now;
        }
    }
}
