mod controller;
mod view;

use controller::Controller;
use view::{View, WorldSettings};

use std::time::Duration;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::{
    event_loop::{EventSettings, Events},
    input::RenderEvent,
    window::WindowSettings,
};
use quicli::prelude::*;
use structopt::StructOpt;
use wireworld::World;

#[derive(StructOpt, Debug, Clone)]
struct Cli {
    /// Number of milliseconds between updates.
    #[structopt(short = "t", long = "tic", default_value = "500")]
    tic: u64,
    /// Path to the file containing the textual representation of the world to run.
    file: String,
    // Quick and easy logging setup you get for free with quicli
    #[structopt(flatten)]
    verbosity: Verbosity,
}

fn main() -> CliResult {
    let args = Cli::from_args();
    args.verbosity.setup_env_logger("gui-piston")?;
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;
    let world = World::parse_str(quicli::fs::read_file(&args.file)?)?;

    // Create an Glutin window.
    let mut window: Window =
        WindowSettings::new("Wireworld", view::size_for(world.width(), world.height()))
            .opengl(opengl)
            .exit_on_esc(true)
            .resizable(false)
            .build()
            .unwrap();

    let mut controller = Controller::new(world, Duration::from_millis(args.tic));
    let settings = WorldSettings::default();
    let view = View::new(settings);

    let mut events = Events::new(EventSettings::new());
    let mut gl = GlGraphics::new(opengl);
    while let Some(e) = events.next(&mut window) {
        controller.event(&e);
        if let Some(args) = e.render_args() {
            gl.draw(args.viewport(), |c, g| {
                view.draw(&controller, &c, g);
            })
        }
    }
    Ok(())
}
