use quicli::prelude::*;
use structopt::StructOpt;
use wireworld::{SerializerOpts, World};

#[derive(Debug, Clone, StructOpt)]
struct Cli {
    /// Number of iteration to run
    #[structopt(short = "n", long = "iter", default_value = "10")]
    iter: u32,
    /// Chars to use for printing, must be of the following form: -c 'C=S' where C is `g`, 'w',
    /// `h`, or `t` for respectively ground, wire, head and tail chars, and S is any utf8 char.
    #[structopt(short = "c")]
    chars: Vec<String>,
    /// Path to the file containing the textual representation of the world to run.
    file: String,
    // Quick and easy logging setup you get for free with quicli
    #[structopt(flatten)]
    verbosity: Verbosity,
}

fn main() -> CliResult {
    let args = Cli::from_args();
    args.verbosity.setup_env_logger("gui-piston")?;
    let opts = serializer_opts(&args.chars);
    debug!("SerializerOpts: {:?}", opts);
    let mut world = World::parse_str(quicli::fs::read_file(&args.file)?)?;
    println!("{}", world.as_serializer_with(&opts));
    for i in 0..args.iter {
        info!("Processing turn {}", i);
        world.process();
        println!("{}", world.as_serializer_with(&opts));
    }
    Ok(())
}

fn serializer_opts(chars: &[String]) -> SerializerOpts {
    if chars.is_empty() {
        return SerializerOpts::default();
    }
    let acc = (
        SerializerOpts::default_ground(),
        SerializerOpts::default_wire(),
        SerializerOpts::default_head(),
        SerializerOpts::default_tail(),
    );
    let (acc, _) =
        chars
            .iter()
            .flat_map(|arg| arg.split('='))
            .fold((acc, None), |(mut acc, prev), arg| {
                debug!("acc: {:?}", acc);
                assert!(arg.len() == 1);
                let curr = arg.chars().next().unwrap();
                if let Some(prev) = prev {
                    match prev {
                        'g' => acc.0 = curr,
                        'w' => acc.1 = curr,
                        'h' => acc.2 = curr,
                        't' => acc.3 = curr,
                        _ => panic!("unknown char: {}", prev),
                    };
                    (acc, None)
                } else {
                    (acc, Some(curr))
                }
            });
    SerializerOpts::new(acc.0, acc.1, acc.2, acc.3)
}
